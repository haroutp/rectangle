﻿using System;

namespace Rectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            RectangleClass rc = new RectangleClass(){
                Character = '#',
                Length = 5,
                Width = 10
            };

            System.Console.WriteLine(rc.Character + " " + rc.Length + " " + rc.Width);
            rc.DrawEmpty();
        }
    }
}
