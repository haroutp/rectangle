using System;

namespace Rectangle
{
    class RectangleClass
    {
        private int length;
        private int width;
        private char character;


        public int Length { 
            get => length; 
            set {
                if(value < 1 || value >= Console.LargestWindowHeight){
                    length = 1;
                }else{
                    length = value;
                }
            }
             
        }
        public int Width { 
            get => width; 
            set {
                if (value < 3 || value >= Console.LargestWindowWidth)
                {
                    width = 3;
                } else{
                width = value;

                }
                
            }
        }

        public char Character { get => character; set => character = value; }

        public void DrawFull(){
            for (int i = 0; i < Length; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    System.Console.Write(Character);
                }
                System.Console.WriteLine();
            }
        }

        public void DrawEmpty(){
            for (int i = 0; i < Length; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if(i == 0 || i == Length - 1){
                        System.Console.Write(character);
                        continue;
                    }
                    if(j == 0 || j == Width - 1){
                        System.Console.Write(character);
                    }else{
                        System.Console.Write(" ");
                    }
                }
                System.Console.WriteLine();
            }
        }

    }
}

